function save() {
    var note = document.getElementById("txt_input").value;
    var savedNotes = localStorage.getItem("notes");

    if (savedNotes) {
        savedNotes = JSON.parse(savedNotes);
    } else {
        savedNotes = [];
    }

    savedNotes.push(note); 
    localStorage.setItem("notes", JSON.stringify(savedNotes));

    renew();
}

function deleteAll() {
    localStorage.clear();
    renew();
}

function renew() {
    var savedNotes = localStorage.getItem("notes");

    if (savedNotes) {
        savedNotes = JSON.parse(savedNotes);
        document.getElementById("txt_data").value = savedNotes.join("\n"); 
    } else {
        document.getElementById("txt_data").value = "";
    }
}
